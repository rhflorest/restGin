package main

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
)

func logiar(context *gin.Context) {
	var login Login
	var error Error
	var exito Exito
	if err := context.ShouldBindJSON(&login); err != nil {
		error.Mensaje = err.Error()
		context.JSON(http.StatusBadRequest, error)
		return
	}

	if login.User != "manu" || login.Password != "123" {
		error.Mensaje = "Usuario o contraseña incorrecta"
		context.JSON(http.StatusUnauthorized, error)
		return
	} else {

		var token = jwt.NewWithClaims(
			jwt.SigningMethodHS256,
			jwt.MapClaims{"name": login.User})

		signedToken, err := token.SignedString([]byte("secreto-muysecreto"))
		if err == nil {
			exito.Token = signedToken
		} else {
			exito.Token = "Error al parsear token"
		}

	}

	context.JSON(http.StatusCreated, exito)
}
