package main

type Login struct {
	User     string `json:"user" binding:"required"`
	Password string `json:"password" binding:"required"`
}
type Error struct {
	Mensaje string `json:"mensaje" binding:"required"`
}
type Exito struct {
	Token string `json:"token" binding:"required"`
}
